(function() {
  var app = angular.module('formCenter', []);

  app.controller('FormController', function($scope, $http) {
    $scope.partTitle = "Start";

    $http.get('mock-response.json').success(function(data, status, headers, config) {
      $scope.partials = data;
    });

    $scope.isActive = function(part) {
      return $scope.partTitle === part;
    };

    $scope.setPart = function(partial, name) {
      $scope.partTitle = name;
    };
  });

  app.controller('partsCtrl', function($scope) {
// I suppose this will control what each input value disables
  });
})();
